<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 0:15
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuAAAPerUnitStrategy;
use Checkout\Strategy\PriceSkuBBBPerUnitStrategy;
use Checkout\Strategy\PriceSkuCCCPerUnitStrategy;
use Checkout\Strategy\PriceSkuDDDPerUnitStrategy;
use Checkout\Strategy\StrategiesPriceList;

/**
 * Class StrategiesPriceListTest
 * @package Tests\Unit\Checkout\Checkout
 */
class StrategiesPriceListTest extends \PHPUnit_Framework_TestCase
{
    public function testAddStrategyParameterShouldBeAnInstanceOfPriceStrategy()
    {
        $this->assertInstanceOf('Checkout\PriceStrategy', new PriceSkuAAAPerUnitStrategy());
    }

    public function testStrategiesPriceListCounterShouldBeUpdatedByNeed()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());

        $this->assertEquals(1, $pricingRules->getStrategyCount());

        $pricingRules->addStrategy(new PriceSkuBBBPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuCCCPerUnitStrategy());

        $this->assertEquals(3, $pricingRules->getStrategyCount());

        $pricingRules->setStrategyCount(6);

        $this->assertEquals(6, $pricingRules->getStrategyCount());
    }

    public function testStrategiesPriceListCanAddElement()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());

        $this->assertEquals(1, $pricingRules->getStrategyCount());

        $pricingRules->addStrategy(new PriceSkuBBBPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuCCCPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuDDDPerUnitStrategy());

        $this->assertEquals(4, $pricingRules->getStrategyCount());
    }

    public function testStrategiesPriceListShouldReturnASpecificElementIndex()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuBBBPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuCCCPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuDDDPerUnitStrategy());

        $obj1 = new PriceSkuBBBPerUnitStrategy;
        $obj2 = $pricingRules->getStrategy(2);
        $this->assertEquals(get_class($obj1), get_class($obj2), 'The strategy is not directed or the same');
    }
}
