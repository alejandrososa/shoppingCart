<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 0:07
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Cart\Line;

/**
 * Class BasicPriceCalculatorTest
 * @package Tests\Unit\Checkout\Checkout
 */
class BasicPriceCalculatorTest extends \PHPUnit_Framework_TestCase
{

    public function testCalculatePriceParameterShouldBeAnInstanceOfLine()
    {
        $line = new Line();

        $this->assertInstanceOf('Checkout\Cart\Line', $line);
    }
}