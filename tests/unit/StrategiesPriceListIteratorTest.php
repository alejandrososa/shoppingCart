<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 0:15
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\StrategiesPriceList;
use Checkout\Strategy\PriceSkuAAAPerUnitStrategy;
use Checkout\Strategy\PriceSkuBBBPerUnitStrategy;
use Checkout\Strategy\PriceSkuCCCPerUnitStrategy;
use Checkout\Strategy\PriceSkuDDDPerUnitStrategy;
use Checkout\Iterators\StrategiesPriceListIterator;

/**
 * Class StrategiesPriceListIteratorTest
 * @package Tests\Unit\Checkout\Checkout
 */
class StrategiesPriceListIteratorTest extends \PHPUnit_Framework_TestCase
{
    public function testStrategiesPriceListIteratorParameterShouldBeAnInstanceOfStrategiesPriceList()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());
        
        $this->assertInstanceOf('Checkout\Strategy\StrategiesPriceList', $pricingRules);
    }

    public function testWhenStartingStrategiesPriceListIteratorCounterShouldBeZero()
    {
        $pricingRules = new StrategiesPriceList();
        $strategiaIterator = new StrategiesPriceListIterator($pricingRules);

        $this->assertFalse($strategiaIterator->hasNextStrategy());
    }

    public function testShouldBringTheCurrentIterationInLoop()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());

        $strategiaIterator = new StrategiesPriceListIterator($pricingRules);

        while ($strategiaIterator->hasNextStrategy()) {
            $strategy = $strategiaIterator->getNextStrategy();
        }

        $obj1 = new PriceSkuAAAPerUnitStrategy;

        $this->assertEquals(get_class($obj1), get_class($strategy), 'The current strategy is not correct');
    }

    public function testIteratorShouldRewind()
    {
        $pricingRules = new StrategiesPriceList();
        $pricingRules->addStrategy(new PriceSkuAAAPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuBBBPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuCCCPerUnitStrategy());
        $pricingRules->addStrategy(new PriceSkuDDDPerUnitStrategy());

        $strategiaIterator = new StrategiesPriceListIterator($pricingRules);

        $counter1 = $counter2 = $counter3 = $total_loops = 0;

        //here makes loop
        while ($strategiaIterator->hasNextStrategy()) {
            $strategiaIterator->getNextStrategy();
            $counter1++;
            $total_loops++;
        }

        self::assertEquals(4, $counter1);
        self::assertEquals(4, $total_loops);

        //here does not loop
        while ($strategiaIterator->hasNextStrategy()) {
            $strategiaIterator->getNextStrategy();
            $counter2++;
            $total_loops++;
        }

        self::assertEquals(0, $counter2);
        self::assertEquals(4, $total_loops);

        //reset counter of list
        //here makes loop
        $strategiaIterator->rewindStrategies();

        while ($strategiaIterator->hasNextStrategy()) {
            $strategiaIterator->getNextStrategy();
            $counter3++;
            $total_loops++;
        }

        self::assertEquals(4, $counter3);
        self::assertEquals(8, $total_loops);
    }
}
