<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 0:07
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Item;
use Checkout\Checkout\BasicCheckout;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class BasicCheckoutTest
 * @package Tests\Unit\Checkout\Checkout
 */
class BasicCheckoutTest extends \PHPUnit_Framework_TestCase
{
    public function testTheEmptyCartTotalShouldBeZero()
    {
        $checkout = BasicCheckout::createBasicCheckout();
        $cart = BasicCart::create();

        $price = $checkout->calculate($cart);

        $this->assertEquals(0, $price, 'Total should be 0');
    }

    public function testShoppingCanAddValueTwoItems()
    {
        $checkout = BasicCheckout::createBasicCheckout();
        $cart = BasicCart::create();

        $cart->addItem(new BasicItem('AAA'), 1);
        $cart->addItem(new BasicItem('BBB'), 1);

        $price = $checkout->calculate($cart);

        $this->assertEquals(155, $price, 'The total is not correct');
    }
}
