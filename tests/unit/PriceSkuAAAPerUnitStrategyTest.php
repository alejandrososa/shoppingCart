<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuAAAPerUnitStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuAAAPerUnitStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuAAAPerUnitStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuAAAPerUnitStrategyIsMatch()
    {
        $strategy = new PriceSkuAAAPerUnitStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 2);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuAAAPerUnitStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuAAAPerUnitStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 1);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(100, $total, 'Price is not correct');
    }
}
