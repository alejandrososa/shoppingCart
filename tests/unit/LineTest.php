<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 19/10/2016
 * Hora: 20:49
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Cart\Line;
use Checkout\Item\BasicItem;

/**
 * Class LineTest
 * @package Tests\Unit\Checkout\Checkout
 */
class LineTest extends \PHPUnit_Framework_TestCase
{
    public function testItemLineShouldBeInstanceOfItem(){

        $item = new BasicItem('AAA');

        $line = new Line();
        $line->item = $item;

        $this->assertInstanceOf('Checkout\Item', $line->item);
    }

    public function testAmountIsGreaterThanZero(){

        $item = new BasicItem('AAA');

        $line = new Line();
        $line->item = $item;
        $line->quantity = 4;

        $this->assertGreaterThan(0, $line->quantity);
    }
}
