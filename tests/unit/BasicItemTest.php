<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 0:07
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Item;
use Checkout\Item\BasicItem;

/**
 * Class BasicItemTest
 * @package Tests\Unit\Checkout\Checkout
 */
class BasicItemTest extends \PHPUnit_Framework_TestCase
{
    public function testEqualsParameterShouldBeAnInstanceOfItem()
    {
        $item1 = new BasicItem('AAA');
        $item2 = new BasicItem('AAA');

        $this->assertInstanceOf('Checkout\Item', $item2);
    }

    public function testCheckIfSkuIsEquals()
    {
        $item1 = new BasicItem('AAA');
        $item2 = new BasicItem('AAA');

        $this->assertTrue($item1->equals($item2));
    }

    public function testCheckIfSkuIsNotEquals()
    {
        $item1 = new BasicItem('AAA');
        $item2 = new BasicItem('BBB');

        $this->assertFalse($item1->equals($item2));
    }

    public function testTheItemNameShouldNotBeEmpty()
    {
        $item = new BasicItem('AAA');
        $this->assertNotEmpty($item->getName());
    }

    public function testTheItemShouldReturnHisName()
    {
        $item = new BasicItem('AAA');
        $this->assertEquals('AAA', $item->getName());
    }
}
