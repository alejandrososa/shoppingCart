<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuCCCPerUnitStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuCCCPerUnitStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuCCCPerUnitStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuCCCPerUnitStrategyIsMatch()
    {
        $strategy = new PriceSkuCCCPerUnitStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('CCC'), 1);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuCCCPerUnitStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuCCCPerUnitStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('CCC'), 1);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(25, $total, 'Price is not correct');
    }
}
