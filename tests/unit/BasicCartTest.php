<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 0:07
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Item;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class BasicCartTest
 * @package Tests\Unit\Checkout\Checkout
 */
class BasicCartTest extends \PHPUnit_Framework_TestCase
{
    public function testCartShouldBeAnInstanceOfAbstractBasicCart()
    {
        $cart = BasicCart::create();

        $this->assertInstanceOf('Checkout\Cart', $cart);
        $this->assertInstanceOf('Checkout\Abstracts\AbstractBasicCart', $cart);
    }

    public function testAddItemParamShouldBeAnInstanceOfItem()
    {
        $cart = BasicCart::create();

        $item = new BasicItem('AAA');

        $this->assertInstanceOf('Checkout\Item', $item);
    }

    public function testShouldANewCartItemHaveAQuantityOfOne()
    {
        $cart = BasicCart::create();

        $cart->addItem(new BasicItem('AAA'), 4);

        $this->assertNotEmpty($cart->getItems(), 'Cart should not be empty');
        $this->assertEquals(1, $cart->countItems(), 'Cart should have at least one item');
    }

    public function testCartShouldHaveThreeItems()
    {
        $cart = BasicCart::create();

        $cart->addItem(new BasicItem('AAA'), 4);
        $cart->addItem(new BasicItem('BBB'), 4);
        $cart->addItem(new BasicItem('CCC'), 2);

        $this->assertNotEmpty($cart->getItems(), 'Cart should not be empty');
        $this->assertEquals(3, $cart->countItems(), 'Cart should have three items');
    }

    public function testAddingItemsToCartShouldAddQuantitiesOfTheSameType(){
        $cart = BasicCart::create();

        $cart->addItem(new BasicItem('AAA'), 4);
        $cart->addItem(new BasicItem('AAA'), 4);
        $cart->addItem(new BasicItem('CCC'), 2);

        $this->assertEquals(2, $cart->countItems(), 'Cart should have 2 items');
    }
}