<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuBBBDiscountStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuBBBDiscountStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuBBBDiscountStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuBBBDiscountStrategyIsMatch()
    {
        $strategy = new PriceSkuBBBDiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('BBB'), 2);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuBBBDiscountStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuBBBDiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('BBB'), 2);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(104.5, $total, 'Discount is not correct');
    }
}
