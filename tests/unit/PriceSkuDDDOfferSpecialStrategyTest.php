<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuDDDOfferSpecialStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuDDDOfferSpecialStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuDDDOfferSpecialStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuDDDOfferSpecialStrategyStrategyIsMatch()
    {
        $strategy = new PriceSkuDDDOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('DDD'), 4);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuDDDOfferSpecialStrategyStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuDDDOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('DDD'), 3);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(50, $total, 'Offer is not correct');
    }

    public function testPriceSkuDDDOfferSpecialStrategyStrategyOnlyAppliesToItemQuantityDiscounts()
    {
        $strategy = new PriceSkuDDDOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('DDD'), 3);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(50, $total, 'Offer is not correct');

        $cart->addItem(new BasicItem('DDD'), 1);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(75, $total, 'Offer is not correct');

        $cart->addItem(new BasicItem('DDD'), 6);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(175, $total, 'Offer is not correct');
    }
}
