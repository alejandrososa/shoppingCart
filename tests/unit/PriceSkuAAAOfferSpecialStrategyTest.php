<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuAAAOfferSpecialStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuAAAOfferSpecialStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuAAAOfferSpecialStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuAAAOfferSpecialStrategyStrategyIsMatch()
    {
        $strategy = new PriceSkuAAAOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 3);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuAAAOfferSpecialStrategyStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuAAAOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 3);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(200, $total, 'Offer is not correct');
    }


    public function testPriceSkuAAAOfferSpecialStrategyStrategyOnlyAppliesToItemQuantityDiscounts()
    {
        $strategy = new PriceSkuAAAOfferSpecialStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 3);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(200, $total, 'Offer is not correct');

        $cart->addItem(new BasicItem('AAA'), 1);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(300, $total, 'Offer is not correct');

        $cart->addItem(new BasicItem('AAA'), 2);
        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(400, $total, 'Offer is not correct');
    }
}
