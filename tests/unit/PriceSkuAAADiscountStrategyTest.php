<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuAAADiscountStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuAAADiscountStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuAAADiscountStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuAAADiscountStrategyIsMatch()
    {
        $strategy = new PriceSkuAAADiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 2);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuAAADiscountStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuAAADiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('AAA'), 2);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }
        
        $this->assertEquals(180, $total, 'Price is not correct');
    }
}
