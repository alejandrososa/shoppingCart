<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 20/10/2016
 * Hora: 2:44
 */

namespace Tests\Unit\Checkout\Checkout;

use Checkout\Strategy\PriceSkuDDDDiscountStrategy;
use Checkout\Cart\BasicCart;
use Checkout\Item\BasicItem;

/**
 * Class PriceSkuDDDDiscountStrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class PriceSkuDDDDiscountStrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testPriceSkuDDDDiscountStrategyIsMatch()
    {
        $strategy = new PriceSkuDDDDiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('DDD'), 2);

        $lines = $cart->getItems();

        $result = false;
        if($strategy->isMatch($lines[0])){
            $result = true;
        }

        $this->assertTrue($result);
    }

    public function testPriceSkuDDDDiscountStrategyShouldGetCorrectPriceTotal()
    {
        $strategy = new PriceSkuDDDDiscountStrategy();

        $cart = BasicCart::create();
        $cart->addItem(new BasicItem('DDD'), 2);

        $lines = $cart->getItems();

        $total = 0;
        if($strategy->isMatch($lines[0])){
            $total = $strategy->calculatePrice($lines[0]);
        }

        $this->assertEquals(45, $total, 'Price is not correct');
    }
}
