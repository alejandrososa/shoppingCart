<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:34
 */

namespace Tests\Unit\Checkout\Checkout;

/**
 * Class StrategyTest
 * @package Tests\Unit\Checkout\Checkout
 */
class StrategyTest extends \PHPUnit_Framework_TestCase
{
    public function testInterfacePriceStrategyExists()
    {
        if (interface_exists('Checkout\PriceStrategy')){
            $this->assertTrue( true );
        }else{
            $this->assertTrue(false,'Interface PriceStrategy not found');
        }
    }
}
