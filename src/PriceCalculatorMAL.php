<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout;

use Checkout\Cart\Line;

/**
 * Interface PriceCalculator
 * @package Checkout
 */
interface PriceCalculator
{
    /**
     * Determine which concrete strategy
     * to pick based on the BasicItem
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line);

    /**
     * Will calculate the correct price
     * @param Line $item
     * @return mixed
     */
    public function calculatePrice(Line $item);
}