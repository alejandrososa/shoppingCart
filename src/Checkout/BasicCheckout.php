<?php

namespace Checkout\Checkout;

use Checkout\Abstracts\AbstractCheckout;
use Checkout\Cart;
use Checkout\Checkout;
use Checkout\Cart\Line;
use Checkout\Strategy\PriceDiscountStrategy;
use Checkout\Strategy\PriceOfferSpecialStrategy;
use Checkout\Strategy\PricePerUnitStrategy;


class BasicCheckout extends AbstractCheckout
{
    /**
     * BasicCheckout constructor.
     */
    public function __construct()
    {
//        $this->priceCalculator = new BasicPriceCalculator();
    }

    /**
     * @return BasicCheckout
     */
    public static function createBasicCheckout()
    {
        return new self();
    }

    /**
     * @param Cart $cart
     * @return float
     */
    public function calculate(Cart $cart)
    {
        $total = 0;
        $lines = $cart->getItems();

        for ($i = 0; $i < $cart->countItems(); $i++){
            $this->setPriceAndDiscount($lines[$i]);
            $this->verifyQuantity($lines[$i]);

            if($lines[$i]->item->getName() == 'DDD') {
                echo '<pre>';
                print_r([__LINE__, __METHOD__, '', $lines[$i], $this->price,
                    $this->discount,
                    $this->strategy,
                    $this->strategy->calculatePrice($lines[$i])]);
                die();
            }

            $total += $this->strategy->calculatePrice($lines[$i]);
//            $total += $this->_priceCalculator->calculatePrice($lines[$i]);
        }
        return $total;
    }

    public function setPriceAndDiscount(Line $line)
    {
        switch ($line->item->getName()){
            case 'AAA':
                $this->price = 100; $this->discount = 10;
                break;
            case 'BBB':
                $this->price = 55; $this->discount = 5;
                break;
            case 'CCC':
                $this->price = 25; $this->discount = 0;
                break;
            case 'DDD':
                $this->price = 25; $this->discount = 10;
                break;
        }
    }

    /**
     * Determine which concrete strategy
     * to pick based on the BasicItem
     * @param Line $line
     */
    public function verifyQuantity(Line $line)
    {
        if ($line->quantity >= 3 && $this->discount > 0){
            $this->setStrategy(new PriceOfferSpecialStrategy($this->price));
        }elseif ($line->quantity > 1 && $line->quantity < 3){
            $this->setStrategy(new PriceDiscountStrategy($this->price, $this->discount));
        }elseif ($line->quantity == 1){
            $this->setStrategy(new PricePerUnitStrategy($this->price));
        }else{
            $this->setStrategy(new PricePerUnitStrategy($this->price));
        }
    }
}