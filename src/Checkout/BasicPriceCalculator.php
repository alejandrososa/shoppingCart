<?php
/**
 * Creado con PhpStorm.
 * Copyright (c) html 2016.
 * Autor: Franklyn Alejandro Sosa Pérez <alesjohnson@hotmail.com>
 * Fecha: 18/10/2016
 * Hora: 13:07
 */

namespace Checkout\Checkout;

use Checkout\Cart\Line;
use Checkout\PriceCalculator;
use Checkout\Iterators\StrategiesPriceListIterator;
use Checkout\Strategy\PriceDiscountStrategy;
use Checkout\Strategy\PriceOfferSpecialStrategy;
use Checkout\Strategy\PricePerUnitStrategy;
use Checkout\Strategy\PriceSkuAAADiscountStrategy;
use Checkout\Strategy\PriceSkuAAAOfferSpecialStrategy;
use Checkout\Strategy\PriceSkuAAAPerUnitStrategy;
use Checkout\Strategy\PriceSkuBBBDiscountStrategy;
use Checkout\Strategy\PriceSkuBBBPerUnitStrategy;
use Checkout\Strategy\PriceSkuCCCPerUnitStrategy;
use Checkout\Strategy\PriceSkuDDDDiscountStrategy;
use Checkout\Strategy\PriceSkuDDDOfferSpecialStrategy;
use Checkout\Strategy\PriceSkuDDDPerUnitStrategy;
use Checkout\Strategy\StrategiesPriceList;

/**
 * Class BasicPriceCalculator
 * @package Checkout\Checkout
 */
class BasicPriceCalculator implements PriceCalculator
{
    /**
     * @var StrategiesPriceList
     */
    private $_pricingRules;

    /**
     * @var StrategiesPriceListIterator
     */
    private $_strategiaIterator;

    /**
     * DefaultPriceCalculator constructor.
     */
    public function __construct(AbstractStrategiesPriceList $priceList)
    {
        //strategies in order of priority
        $this->_pricingRules = $priceList;
        $this->_pricingRules->addStrategy(new PriceOfferSpecialStrategy());
        $this->_pricingRules->addStrategy(new PriceDiscountStrategy());
        $this->_pricingRules->addStrategy(new PricePerUnitStrategy());

        $this->_strategiaIterator = new StrategiesPriceListIterator($this->_pricingRules);
    }

    /**
     * Will calculate the correct price
     * @param Line $line
     * @return mixed|integer
     */
    public function calculatePrice(Line $line)
    {
        $this->_strategiaIterator->rewindStrategies();

        while ($this->_strategiaIterator->hasNextStrategy()) {
            $strategy = $this->_strategiaIterator->getNextStrategy();
            if($strategy->isMatch($line)){
                return $strategy->calculatePrice($line);
            }
        }
    }

    /**
     * Determine which concrete strategy
     * to pick based on the BasicItem
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        // TODO: Implement isMatch() method.
    }
}