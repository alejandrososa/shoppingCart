<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout;

use Checkout\Cart\Line;

/**
 * Interface PriceStrategy
 * @package Checkout
 */
interface PriceStrategy
{
    /**
     * Calculate price of BasicItem
     * @param Line $line
     * @return mixed
     */
    public function calculatePrice(Line $line);
}