<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Strategy;

use Checkout\PriceStrategy;
use Checkout\Cart\Line;

/**
 * Class PriceSkuAAADiscountStrategy
 * @package Checkout\Strategy
 */
class PriceSkuAAADiscountStrategy implements PriceStrategy
{
    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $discount;

    /**
     * PriceSkuAAADiscountStrategy constructor.
     */
    public function __construct()
    {
        $this->price    = 100;
        $this->discount = 10;
    }

    /**
     * Determine which concrete strategy
     * to pick based on the Line
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        if(strcmp($line->item->getName(), 'AAA') == 0){
            return $line->quantity == 2 ? true : false;
        }
        
        return false;
    }

    /**
     *
     * Calculate price of Line
     * @param Line $line
     * @return integer
     */
    public function calculatePrice(Line $line)
    {
        $amount_discount = $this->price * ($this->discount / 100);

        return $line->quantity * ($this->price - $amount_discount);
    }
}