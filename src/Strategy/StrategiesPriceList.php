<?php
/**
 * Creado con PhpStorm.
 * Copyright (c) html 2016.
 * Autor: Franklyn Alejandro Sosa Pérez <alesjohnson@hotmail.com>
 * Fecha: 18/10/2016
 * Hora: 12:54
 */

namespace Checkout\Strategy;

use Checkout\Abstracts\AbstractStrategiesPriceList;
use Checkout\PriceStrategy;

/**
 * Class StrategiesPriceList
 * @package Checkout\Strategy
 */
class StrategiesPriceList extends AbstractStrategiesPriceList
{
    /**
     * Get specific strategy by index
     * @param $strategyNumberToGet
     * @return PriceStrategy|null
     */
    public function getStrategy($strategyNumberToGet)
    {
        if(is_numeric($strategyNumberToGet) && ($strategyNumberToGet <= $this->getStrategyCount())){
            return new $this->strategies[$strategyNumberToGet];
        }else{
            return null;
        }
    }

    /**
     * Add strategy to priceList
     * @param PriceStrategy $strategy_in
     * @return int
     */
    public function addStrategy(PriceStrategy $strategy_in)
    {
        if($strategy_in instanceof PriceStrategy){
            $this->setStrategyCount($this->getStrategyCount() + 1);
            $this->strategies[$this->getStrategyCount()] = $strategy_in;

            return $this->getStrategyCount();
        }
    }
}