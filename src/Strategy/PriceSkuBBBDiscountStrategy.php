<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Strategy;

use Checkout\PriceStrategy;
use Checkout\Cart\Line;

/**
 * Class PriceSkuBBBDiscountStrategy
 * @package Checkout\Strategy
 */
class PriceSkuBBBDiscountStrategy implements PriceStrategy
{
    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $discount;

    /**
     * PriceSkuBBBDiscountStrategy constructor.
     */
    public function __construct()
    {
        $this->price    = 55;
        $this->discount = 5;
    }

    /**
     * Determine which concrete strategy
     * to pick based on the Line
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        if(strcmp($line->item->getName(), 'BBB') == 0){
            return $line->quantity >= 2 ? true : false;
        }

        return false;
    }

    /**
     * Calculate price of Line
     * @param Line $line
     * @return integer
     */
    public function calculatePrice(Line $line)
    {
        $amount_discount = $this->price * ($this->discount / 100);

        return $line->quantity * ($this->price - $amount_discount);
    }
}