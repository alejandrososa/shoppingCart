<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Strategy;

use Checkout\Item\BasicItem;
use Checkout\PriceStrategy;
use Checkout\Cart\Line;

/**
 * Class PriceOfferSpecialStrategy
 * @package Checkout\Strategy
 */
class PriceOfferSpecialStrategy implements PriceStrategy
{
    /**
     * @var integer
     */
    private $price;

    /**
     * PriceOfferSpecialStrategy constructor.
     * @param $price
     */
    public function __construct($price)
    {
        $this->price = $price;
    }

    /**
     * Determine which concrete strategy
     * to pick based on the Line
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        if(strcmp($line->item->getName(), 'AAA') == 0){
            return $line->quantity >= 3 ? true : false;
        }

        return false;
    }

    /**
     *
     * Calculate price of Line
     * @param Line $line
     * @return integer
     */
    public function calculatePrice(Line $line)
    {
        $times_discount_applies = intval($line->quantity / 3);
        $total_unit_pay_only    = (3 * $times_discount_applies) - $times_discount_applies;
        $unit_with_offert       = (3 * $times_discount_applies);
        $unit_without_offert    = $line->quantity - $unit_with_offert;
        $price_real_unit        = (($this->price * $total_unit_pay_only) / $unit_with_offert);

        $amount_unit_with_offer = ($price_real_unit * $unit_with_offert);
        $amount_unit_without_offer = ($this->price * $unit_without_offert);

        return ($amount_unit_with_offer + $amount_unit_without_offer);
    }
}