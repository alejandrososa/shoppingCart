<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Strategy;

use Checkout\PriceStrategy;
use Checkout\Cart\Line;

/**
 * Class PriceDiscountStrategy
 * @package Checkout\Strategy
 */
class PriceDiscountStrategy implements PriceStrategy
{
    /**
     * @var integer
     */
    private $price;

    /**
     * @var integer
     */
    private $discount;

    /**
     * PriceDiscountStrategy constructor.
     * @param $price
     * @param $discount
     */
    public function __construct($price, $discount)
    {
        $this->price    = $price;
        $this->discount = $discount;
    }

    /**
     * Determine which concrete strategy
     * to pick based on the Line
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        if(strcmp($line->item->getName(), 'AAA') == 0){
            return $line->quantity == 2 ? true : false;
        }
        
        return false;
    }

    /**
     *
     * Calculate price of Line
     * @param Line $line
     * @return integer
     */
    public function calculatePrice(Line $line)
    {
        $amount_discount = $this->price * ($this->discount / 100);

        return $line->quantity * ($this->price - $amount_discount);
    }
}