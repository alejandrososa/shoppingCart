<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Strategy;

use Checkout\Item\BasicItem;
use Checkout\PriceStrategy;
use Checkout\Cart\Line;

/**
 * Class PriceSkuCCCPerUnitStrategy
 * @package Checkout\Strategy
 */
class PriceSkuCCCPerUnitStrategy implements PriceStrategy
{
    /**
     * @var integer
     */
    private $price;

    /**
     * PriceSkuCCCPerUnitStrategy constructor.
     */
    public function __construct()
    {
        $this->price = 25;
    }

    /**
     * Determine which concrete strategy
     * to pick based on the Line
     * @param Line $line
     * @return boolean
     */
    public function isMatch(Line $line)
    {
        return strcmp($line->item->getName(), 'CCC') !== 0 ? false : true;
    }

    /**
     * Calculate price of Line
     * @param Line $line
     * @return mixed
     */
    public function calculatePrice(Line $line)
    {
        return $line->quantity * $this->price;
    }
}