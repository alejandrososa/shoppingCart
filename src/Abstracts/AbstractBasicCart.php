<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 19/10/2016
 * Hora: 22:54
 */

namespace Checkout\Abstracts;

use Checkout\Cart;

/**
 * Class AbstractBasicCart
 * @package Checkout\Abstracts
 */
abstract class AbstractBasicCart implements Cart
{
    /**
     * @var Line
     */
    protected $_line;

    /**
     * @var Line[]
     */
    protected $_listItem = [];

    /**
     * Count items
     * @return integer
     */
    abstract public function countItems();

    /**
     * Get all items
     * @return Line[]
     */
    abstract public function getItems();
}