<?php
/**
 * Creado con PhpStorm.
 * Copyright (c) html 2016.
 * Autor: Franklyn Alejandro Sosa Pérez <alesjohnson@hotmail.com>
 * Fecha: 20/10/2016
 * Hora: 10:22
 */

namespace Checkout\Abstracts;

use Checkout\PriceStrategy;

/**
 * Class AbstractStrategiesPriceListIterator
 * @package Checkout\Abstracts
 */
abstract class AbstractStrategiesPriceListIterator
{
    /**
     * @var StrategiesPriceList
     */
    protected $strategiesList;

    /**
     * @var int
     */
    protected $currentStrategy;

    /**
     * Get next Strategy
     * @return PriceStrategy|null
     */
    abstract public function getNextStrategy();

    /**
     * Check if have another strategy
     * @return bool
     */
    abstract public function hasNextStrategy();

    /**
     * Rewind list stategies
     */
    abstract public function rewindStrategies();
}