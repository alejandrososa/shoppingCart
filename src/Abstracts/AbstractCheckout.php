<?php
/**
 * Creado con PhpStorm.
 * entrevista_atrapalo
 * Desarrollador: Alejandro Sosa
 * Fecha: 18/10/2016
 * Hora: 2:37
 */

namespace Checkout\Abstracts;

use Checkout\Cart\Line;
use Checkout\Checkout;
use Checkout\PriceStrategy;

/**
 * Interface AbstractPriceCalculator
 * @package Checkout
 */
abstract class AbstractCheckout implements Checkout
{
    /**
     * @var PriceStrategy
     */
    protected $strategy;

    /**
     * @var double
     */
    protected $price;

    /**
     * @var int
     */
    protected $discount;

    /**
     * Determine which concrete strategy
     * to pick based on the BasicItem
     * @param Line $line
     */
    abstract function verifyQuantity(Line $line);

    abstract function setPriceAndDiscount(Line $line);

    /**
     * @param PriceStrategy $strategy
     */
    public function setStrategy(PriceStrategy $strategy){
        $this->strategy = $strategy;
    }
}