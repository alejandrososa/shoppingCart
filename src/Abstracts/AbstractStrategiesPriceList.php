<?php
/**
 * Creado con PhpStorm.
 * Copyright (c) html 2016.
 * Autor: Franklyn Alejandro Sosa Pérez <alesjohnson@hotmail.com>
 * Fecha: 20/10/2016
 * Hora: 10:17
 */

namespace Checkout\Abstracts;
use Checkout\PriceStrategy;

/**
 * Class AbstractStrategiesPriceList
 * @package Checkout\Abstracts
 */
abstract class AbstractStrategiesPriceList
{
    /**
     * @var PriceStrategy[]
     */
    protected $strategies = [];

    /**
     * @var int
     */
    protected $strategyCount = 0;

    /**
     * Get total strategies add
     * @return int
     */
    public function getStrategyCount()
    {
        return $this->strategyCount;
    }

    /**
     * Set counter of priceList
     * @param $newCount
     */
    public function setStrategyCount($newCount)
    {
        $this->strategyCount = $newCount;
    }

    /**
     * Get specific strategy by index
     * @param $strategyNumberToGet
     * @return PriceStrategy|null
     */
    abstract public function getStrategy($strategyNumberToGet);

    /**
     * Add strategy to priceList
     * @param PriceStrategy $strategy_in
     * @return int
     */
    abstract public function addStrategy(PriceStrategy $strategy_in);
}