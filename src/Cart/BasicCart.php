<?php

namespace Checkout\Cart;

use Checkout\Abstracts\AbstractBasicCart;
use Checkout\Cart;
use Checkout\Item;

class BasicCart extends AbstractBasicCart
{
    /**
     * @return BasicCart
     */
    public static function create()
    {
        return new self();
    }

    /**
     * @param Item $item
     * @param int $qty
     */
    public function addItem(Item $item, $qty)
    {
        $existingItems = [];
        foreach ($this->_listItem as $line){
            $existingItems[] = $line->item->getName();
        }

        if(in_array($item->getName(), $existingItems)) {
            foreach ($this->_listItem as $line){
                if($item->equals($line->item)){
                    $line->quantity += $qty;
                }
            }
        }else{
            $this->_line = new Line();
            $this->_line->item = $item;
            $this->_line->quantity = $qty;
            $this->_listItem[] = $this->_line;
        }
    }

    /**
     * Count items
     * @return integer
     */
    public function countItems()
    {
        return count($this->_listItem);
    }

    /**
     * Get all items
     * @return Line[]
     */
    public function getItems()
    {
        return $this->_listItem;
    }
}