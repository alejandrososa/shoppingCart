<?php
/**
 * Creado con PhpStorm.
 * Copyright (c) html 2016.
 * Autor: Franklyn Alejandro Sosa Pérez <alesjohnson@hotmail.com>
 * Fecha: 18/10/2016
 * Hora: 12:50
 */

namespace Checkout\Iterators;

use Checkout\Abstracts\AbstractStrategiesPriceListIterator;
use Checkout\Strategy\StrategiesPriceList;

/**
 * Class StrategiesPriceListIterator
 * @package Checkout\Iterators
 */
class StrategiesPriceListIterator extends AbstractStrategiesPriceListIterator
{
    /**
     * StrategiesPriceListIterator constructor.
     * @param StrategiesPriceList $strategiesList_in
     */
    public function __construct(StrategiesPriceList $strategiesList_in)
    {
        if($strategiesList_in instanceof StrategiesPriceList){
            $this->strategiesList = $strategiesList_in;
        }
        $this->currentStrategy = 0;
    }

    /**
     * Get next Strategy
     * @return PriceStrategy|null
     */
    public function getNextStrategy() {
        if ($this->hasNextStrategy()) {
            return $this->strategiesList->getStrategy(++$this->currentStrategy);
        } else {
            return null;
        }
    }

    /**
     * Check if have another strategy
     * @return bool
     */
    public function hasNextStrategy() {
        return $this->strategiesList->getStrategyCount() > $this->currentStrategy ? true : false;
    }

    /**
     * Rewind list stategies
     */
    public function rewindStrategies(){
        $this->currentStrategy = 0;
    }
}